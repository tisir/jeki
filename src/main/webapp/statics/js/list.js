var docList = [
    {
        id: 'start',
        title: '1. 开始使用',
        list: [
            {
                id:'start',
                title: '1.1 开始使用ueditor'
            },
            {
                id:'toolbar',
                title: '1.2 定制工具栏图标'
            },
            {
                id: 'config',
                title: '1.3 前端配置项说明'
            },
            {
                id: 'dir',
                title: '1.4 目录介绍'
            },
            {
                id: 'submit',
                title: '1.5 提交表单'
            },
            {
                id: 'uparse',
                title: '1.6 编辑内容展示'
            },
            {
                id: 'patch',
                title: '1.7 如何使用补丁文件'
            }//,
//            {
//                id: 'shortkey',
//                title: '1.8 设置快捷键'
//            },
//            {
//                id: 'event',
//                title: '1.9 监听编辑器事件'
//            },
//            {
//                id: 'filter',
//                title: '1.10 粘贴过滤器(黑白名单)'
//            }
        ]
    },
    {
        id: 'server',
        title: '2. 后端配置',
        list: [
            {
                id:'deploy',
                title: '2.1 后端部署说明'
            },
            {
                id:'php',
                title: '2.2 PHP 使用说明'
            },
            {
                id:'asp',
                title: '2.3 ASP 使用说明'
            },
            {
                id:'net',
                title: '2.4 ASP.NET 使用说明'
            },
            {
                id:'jsp',
                title: '2.5 JSP 使用说明'
            }
        ]
    },
];